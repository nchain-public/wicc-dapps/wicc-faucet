import axios from "axios";

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.timeout = 30000;

let appLang;
let lang = window.navigator.language
if (/zh/.test(lang)) {
    appLang = 'zh-CHS'
} else {
    appLang = 'en'
}
axios.defaults.headers.common['lang'] = appLang
// 添加请求拦截器
axios.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    // config.headers['user_timezone'] = `${getLocalTimeZone()}`;
    // if (config.url.indexOf('wiccdev.org/v2/api/account/getaccountinfo') === -1) {
    //     config.headers['user_timezone'] = `${getLocalTimeZone()}`;
    // }
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
});

export default {
    get(url, data, config) {
        let httpConfig = {}
        if (config) {
            for (let item in config) {
                httpConfig[item] = config[item]
            }
        }
        httpConfig.params = data
        return new Promise((resolve, reject) => {
            axios.get(url + "?ts=" + new Date().getTime(), httpConfig)
                .then(response => {
                    resolve(response.data);
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    post(url, data, config) {
        return new Promise((resolve, reject) => {
            axios.post(url, data, config)
                .then(response => {
                    resolve(response.data);
                })
                .catch((error) => {
                    reject(error)
                })
        })
    }
}
