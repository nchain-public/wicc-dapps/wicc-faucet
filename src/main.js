// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import api from './assets/script/api'
import baasUrl from '@/assets/script/baas.js'
// import $ from 'jquery'
// import 'bootstrap3/dist/css/bootstrap.css'
// import 'bootstrap3/dist/js/bootstrap.js'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import VueClipboard from 'vue-clipboard2'
 
Vue.use(VueClipboard)
Vue.use(ElementUI)
Vue.config.productionTip = false
Vue.prototype.$http= api;
Vue.prototype.$baasUrl= baasUrl;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
